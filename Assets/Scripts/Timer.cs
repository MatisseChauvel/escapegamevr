using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class Timer : MonoBehaviour
{
    public GameObject timer;
    public TextMeshProUGUI textMeshPro;
    public float min;
    public float sec;
    float time = 0.0f;
    // Start is called before the first frame update
    void Start()
    {
        textMeshPro = timer.GetComponent<TextMeshProUGUI>();
    }

    // Update is called once per frame
    void Update()
    {
        //On lance et affiche le chronomètre au placement du joueur
        time += Time.deltaTime;
        min = (int)(time / 60f);
        sec = (int)(time % 60f);
        textMeshPro.text = min.ToString("00") + ":" + sec.ToString("00");
        if (min == 10)
            SceneManager.LoadScene("GameOver");
    }
}