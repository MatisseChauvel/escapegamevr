using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class FinishDoor : MonoBehaviour
{
    [SerializeField] private Animator doorAnimator;

    public void OnDoorUnlock(SelectEnterEventArgs args)
    {
        if (args.interactableObject.transform.tag == "key")
        {
            doorAnimator.SetTrigger("openDoor");
        }
    }
}
