using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;
using System;

public class DetectColor : MonoBehaviour
{
    public GameObject painting;
    FallingPainting script;

    private void Start()
    {
        script = painting.GetComponent<FallingPainting>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == transform.gameObject.tag)
        {
            if (other.gameObject.tag == "Pink")
            {
                Debug.Log("test");
                script.pinkTrue();
            }
            if (other.gameObject.tag == "Green")
                script.greenTrue();
            if (other.gameObject.tag == "Yellow")
                script.yellowTrue();
            if (other.gameObject.tag == "Red")
                script.redTrue();
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == transform.gameObject.tag)
        {
            if (other.gameObject.tag == "Pink")
                script.pinkFalse();
            if (other.gameObject.tag == "Green")
                script.greenFalse();
            if (other.gameObject.tag == "Yellow")
                script.yellowFalse();
            if (other.gameObject.tag == "Red")
                script.redFalse();
        }
    }
}
