using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CutArmory : MonoBehaviour
{
    public GameObject texte;
    private int count = 0;
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "saw" && count == 0)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z + 0.5f);
            count++;
            texte.SetActive(true);
        }
    }
}
