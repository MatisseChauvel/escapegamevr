using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DestroyDoor : MonoBehaviour
{
    public GameObject txt;
    public void Cassage()
    {
        Destroy(transform.gameObject);
        txt.SetActive(true);
    }

    public void Fail()
    {
        SceneManager.LoadScene("GameOver");
    }
}
