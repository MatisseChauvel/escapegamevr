using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallingPainting : MonoBehaviour
{
    public bool pink;    
    public bool green;
    public bool red;
    public bool yellow;
    private Rigidbody rb;
    public GameObject texte;
    private void Start()
    {
        pink = false;
        green = false;
        yellow = false;
        red = false;
        rb = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        if (pink == true && green == true && yellow == true && red == true)
        {
            Debug.Log("Chute � l'arri�re !");
            rb.useGravity = true;
            texte.SetActive(true);
        }
    }

    public void pinkTrue()
    {
        pink = true;
    }
    public void greenTrue()
    {
        green = true;
    }
    public void yellowTrue()
    {
        yellow = true;
    }
    public void redTrue()
    {
        red = true;
    }

    public void pinkFalse()
    {
        pink = false;
    }
    public void greenFalse()
    {
        green = false;
    }
    public void yellowFalse()
    {
        yellow = false;
    }
    public void redFalse()
    {
        red = false;
    }
}
